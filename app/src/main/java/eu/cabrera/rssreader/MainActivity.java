package eu.cabrera.rssreader;

import android.content.Intent;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

import eu.cabrera.rssreader.tools.PullToRefreshListView;

/*
 *
 * Enoncé du test technique Android :
 *
 * - Prendre un flux RSS de votre choix
 * - Parser ce flux
 * - Afficher une liste d'articles correspondant(titre+image), avec Pull-to-refresh
 * - Accès à une page détails qui affiche le titre, la date, la description et l'image de l'article
 *
 * Contrainte : les données doivent être accessibles hors ligne.
 */

public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener, PullToRefreshListView.OnRefreshListener {

    private ProgressBar progressBar;
    private PullToRefreshListView pullToRefreshListView;

   RssAdapter adapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // https://github.com/nostra13/Android-Universal-Image-Loader
        //  displayImage(...) call if no options will be passed to this method
        DisplayImageOptions displayimageOptions = new DisplayImageOptions.Builder().cacheInMemory().cacheOnDisc().build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).
                defaultDisplayImageOptions(displayimageOptions).build();

        ImageLoader.getInstance().init(config);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pullToRefreshListView);
        pullToRefreshListView.setOnItemClickListener(this);
        pullToRefreshListView.setOnRefreshListener(this);
        startService();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void startService() {
        Intent intent = new Intent(this, RssService.class);
        intent.putExtra(RssService.RECEIVER, resultReceiver);
        this.startService(intent);
    }

    /**
     * notification du service lorsqu'il a fini
     */
    private final ResultReceiver resultReceiver = new ResultReceiver(new Handler()) {

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            progressBar.setVisibility(View.GONE);
            List<RssItem> items = (List<RssItem>) resultData.getSerializable(RssService.ITEMS);

            if (items != null) {
                adapter = new RssAdapter(getApplicationContext(), items);
                pullToRefreshListView.setAdapter(adapter);
                pullToRefreshListView.onRefreshComplete();
            }
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        RssItem item = (RssItem) adapter.getItem((int)id);

        Intent intentDetails;
        intentDetails = new Intent(this, ItemDetailsActivity.class);
        intentDetails.putExtra("title", item.getTitle());
        intentDetails.putExtra("pubDate", item.getPubDate());
        intentDetails.putExtra("description", item.getDescription());
        intentDetails.putExtra("image", item.getImage());

        startActivity(intentDetails);
    }

    @Override
    public void onRefresh() {

        startService();
    }
}
