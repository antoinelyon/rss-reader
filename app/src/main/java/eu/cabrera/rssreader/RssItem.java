package eu.cabrera.rssreader;

/**
 * Created by antoine on 29/10/14.
 *
 */
public class RssItem {

    private final String title;
    private final String link;
    private final String description;
    private final String pubDate;
    private final String image;

    public RssItem(String title, String link, String description, String pubDate, String image) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.pubDate = pubDate;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getImage() {
        return image;
    }
}
