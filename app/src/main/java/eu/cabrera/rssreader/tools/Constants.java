package eu.cabrera.rssreader.tools;

/**
 * Created by antoine on 29/10/14.
 */
public class Constants {

    public static final String TAG = "RssApp";
    public static final String RSS_URL = "http://www.lemonde.fr/m-actu/rss_full.xml" ; //http://www.lemonde.fr/m-actu/rss_full.xml"; // http://www.leparisien.fr/actualites-a-la-une.rss.xml

    public static final String RSS_CACHE = "cacheRss.xml";
}
