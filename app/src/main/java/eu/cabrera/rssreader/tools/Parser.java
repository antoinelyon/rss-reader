package eu.cabrera.rssreader.tools;

/**
 * Created by antoine on 29/10/14.
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

import eu.cabrera.rssreader.RssItem;

public class Parser {

    private final String ns = null;

    public List<RssItem> parse(InputStream inputStream) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(inputStream, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            inputStream.close();
        }
    }

    private List<RssItem> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, null, "rss");
        String title = null;
        String link = null;
        String description = null;
        String pubDate = null;
        String image = null;

        List<RssItem> items = new ArrayList<RssItem>();
        int numitem = 0;

        boolean insideItems = false;


        while (parser.next() != XmlPullParser.END_DOCUMENT) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("item") ) {
                if ( !insideItems) {
                    insideItems = true; // On rentre dans les items
                }
                else { // On sort d'un item
                    if (title != null && link != null) {

                        RssItem item = new RssItem(title, link, description, pubDate, image);
                        items.add(item);

                        title = null;
                        link = null;
                        description = null;
                        pubDate = null;
                        image = null;
                    }
                    insideItems = false;
                }
            }
            else {
                if (name.equals("title")) {
                    title = readTitle(parser);
                } else if (name.equals("description")) {
                    description = readDescription(parser);
                } else if (name.equals("link")) {
                    link = readLink(parser);
                }  else if (name.equals("enclosure")) {
                    image = readImage(parser);
                } else if (name.equals("pubDate")) {
                    pubDate = readPubDate(parser);
                }

            }
        }
        return items;
    }

    private String readLink(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "link");
        String link = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "link");
        return link;
    }

    private String readTitle(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "title");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "title");
        return title;
    }

    private String readDescription(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "description");
        String description = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "description");
        return description;
    }

    private String readPubDate(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "pubDate");
        String pubDate = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "pubDate");
        return pubDate;
    }

    private String readImage(XmlPullParser parser) throws XmlPullParserException, IOException {
        String image = null;
        if ( parser.getAttributeValue(null,"type").startsWith("image/")){
            image =  parser.getAttributeValue(null,"url");
        }

        return image;
    }

    // Extraction des valeurs
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
}
