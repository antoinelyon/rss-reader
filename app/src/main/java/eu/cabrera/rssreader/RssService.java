package eu.cabrera.rssreader;

/**
 * Created by antoine on 29/10/14.
 *
 * Service de Récupération du FLUX RSS
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import eu.cabrera.rssreader.tools.Constants;
import eu.cabrera.rssreader.tools.Parser;


public class RssService extends IntentService {

    public static final String ITEMS = "items";
    public static final String RECEIVER = "receiver";


    public RssService() {
        super("RssService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        List<RssItem> rssItems = null;

        InputStream inputStream = getInputStream(Constants.RSS_URL) ;

        // On teste si l'accès aux données est possible

        boolean sauvegardelocale = true ;

        try {
            final byte[] buffert = new byte[10];
            int readt = inputStream.read(buffert);
        }
        catch( Exception e){
            sauvegardelocale = false ;
        }

        if (sauvegardelocale) {
            try {

                 inputStream = getInputStream(Constants.RSS_URL);

                // Sauvegarde locale
                try {
                    final File file = new File(getCacheDir(), Constants.RSS_CACHE);

                    final OutputStream output = new FileOutputStream(file);
                    try {
                        try {
                            final byte[] buffer = new byte[1024];
                            int read;

                            while ((read = inputStream.read(buffer)) != -1)
                                output.write(buffer, 0, read);

                            output.flush();
                        } finally {
                            output.close();

                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                } finally {

                }

            } catch (IOException e) {

            }

        }

        // Si on a pu sauvegarder, on  repart du fichier hors ligne
        // Sinon on tente de lire un ancien fichier s'il existe...

        try {
            final File file = new File(getCacheDir(), Constants.RSS_CACHE);
            InputStream raw = new FileInputStream(file);
            Parser parser = new Parser();
            rssItems = parser.parse(raw);
        } catch (XmlPullParserException e) {

        } catch (Exception e) {

        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(ITEMS, (Serializable) rssItems);
        ResultReceiver receiver = intent.getParcelableExtra(RECEIVER);
        receiver.send(0, bundle);
    }

    public InputStream getInputStream(String link) {
        try {
            URL url = new URL(link);
            return url.openConnection().getInputStream();
        } catch (IOException e) {

            return null;
        }
    }
}
