package eu.cabrera.rssreader;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;


public class ItemDetailsActivity extends ActionBarActivity {

    ImageView imageDetail ;

    ImageLoader imageLoader;
    ImageLoadingListener animateFirstDisplayListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getApplicationContext()));


        TextView titleDetail = (TextView) findViewById(R.id.titleDetail);
        TextView dateDetail = (TextView) findViewById(R.id.dateDetail);
        TextView descriptionDetail = (TextView) findViewById(R.id.descriptionDetail);
         imageDetail = (ImageView) findViewById(R.id.imageDetail);

        Intent intent = getIntent();


        Bundle extras  =  intent.getExtras();

        String title = extras.getString("title");
        String datePub = extras.getString("pubDate");
        String description = extras.getString("description");
        String imageURL = extras.getString("image");

        titleDetail.setText(title);
        dateDetail.setText(datePub);
        descriptionDetail.setText(Html.fromHtml(description));

        imageDetail.setTag(imageURL);


        imageLoader.displayImage(imageURL, imageDetail, animateFirstDisplayListener);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.item_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
